import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotFoundComponent } from './pages/error-pages/not-found/not-found.component';
import { AuthGuard } from '@app/core/guards/auth.guard';
import { SigninGuard } from '@app/core/guards/signin.guard';

const appRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule),
    canActivateChild: [AuthGuard]
  },
  {
    path: 'signin',
    loadChildren: () => import('./pages/signin/signin.module').then(m => m.SigninModule),
    canActivate: [SigninGuard]
  },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404', pathMatch: 'full' },
  // {
  //   path: '',
  //   component: NavbarComponent,
  //   children: [
  //     { path: '', loadChildren: './pages/pages.module#PagesModule' },
  //     { path: '404', component: NotFoundComponent },
  //     { path: '**', component: ErrorPagesComponent },
  //   ]
  // }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(appRoutes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
