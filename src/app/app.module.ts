import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { Router } from '@angular/router';

import { AppComponent } from './app.component';

import { NotFoundComponent } from './pages/error-pages/not-found/not-found.component';
import { ErrorPagesComponent } from './pages/error-pages/error-pages.component';
import { NavbarComponent } from './modules/navbar/navbar.component';

import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AuthInterceptor } from '@app/core/interceptors/auth.interceptor';

import { PostsCreateModalComponent } from '@app/modules/shared/posts/posts-create-modal/posts-create-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    ErrorPagesComponent,
    NavbarComponent,
    PostsCreateModalComponent
  ],
  entryComponents: [
    PostsCreateModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
    AppRoutingModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(router: Router) {
    const replacer = (key, value) => (typeof value === 'function') ? value.name : value;
    // console.log('Routes: ', JSON.stringify(router.config, replacer, 2));
  }
}
