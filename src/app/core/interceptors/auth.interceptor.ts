import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { tap } from 'rxjs/internal/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let sessionToken = localStorage.getItem('sessionToken');
    if (!sessionToken) {
      sessionToken = '';
    }

    const authReq = req.clone({
      headers: req.headers.set('Session', sessionToken)
    });

    return next.handle(authReq).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
          // console.log('Server response');
        }
      }, err => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) console.error('Unauthorized');
          if (err.status === 400) console.error('400', err.error.error);
        }
      })
    );
  }
}
