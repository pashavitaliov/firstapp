import { Injectable } from '@angular/core';
import { USER } from '@app/mock-user';
import { NavigationCancel, Router } from '@angular/router';
import { Observable, of, Subscription } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  redirectUrl: string;

  cancelUrlSubscription$: Subscription;

  constructor(
    private router: Router,
    private http: HttpClient,
  ) {
    this.chekingCancelUrl();
  }

  login(user): Observable<any> {
    return this.http.post<any>('https://reqres.in/api/login', user).pipe(
      tap(responce => {
        if (responce && responce.token) {
          localStorage.setItem('sessionToken', responce.token);
        }

        if (this.redirectUrl) {
          this.redirectToUrl();
          return;
        }

        this.router.navigate(['']);
      }),
      catchError(this.handleError('login', []))
    );
  }

  logout() {
    localStorage.removeItem('sessionToken');
    this.redirectUrl = this.router.url;
    this.router.navigate(['/signin']);
  }

  private chekingCancelUrl() {
    this.cancelUrlSubscription$ = this.router.events.subscribe(events => {
      if (events instanceof NavigationCancel) {
        this.redirectUrl = events.url;
      }
    });
  }

  private redirectToUrl() {
    this.router.navigate([this.redirectUrl]);
    this.redirectUrl = '';
    this.cancelUrlSubscription$.unsubscribe();
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
