import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs/index';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  loading = new BehaviorSubject(false);

  constructor() { }

  getIsLoading(): Observable<boolean> {
    return this.loading.asObservable();
  }

  setIsLoading(value: boolean) {
    this.loading.next(value);
  }
}
