import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, } from 'rxjs/index';
import { map, tap } from 'rxjs/internal/operators';

import { Comment } from '@app/models/comment';
import { Post } from '@app/models/post';
import { LoadingService } from '@app/core/services/loading.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: 'my-auth-token'
  })
};

const urlOptions = {
  comments: 'https://jsonplaceholder.typicode.com/comments',
  posts: 'https://jsonplaceholder.typicode.com/posts',

};

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  postsSubject = new BehaviorSubject([] as Post[]);
  singlePostSubject = new BehaviorSubject({} as any as Post);
  isEndPostsSubject = new BehaviorSubject(false);

  numberSearchesPosts = new BehaviorSubject(0);
  numberDisplayedPosts = new BehaviorSubject(10);

  commentsSubject = new BehaviorSubject([] as Comment[]);
  isEndCommentsSubject = new BehaviorSubject(false);

  numberSearchingComments = new BehaviorSubject(0);
  numberDisplayedComments = new BehaviorSubject(3);

  constructor(
    private http: HttpClient,
    private router: Router,
    private loading: LoadingService
  ) {
  }

  setNumberSearchesPosts(value) {
    this.numberSearchesPosts.next(value);
  }
  getNumberSearchesPosts(): Observable<number> {
    return this.numberSearchesPosts.asObservable();
  }
  getNumberDisplayedPosts(): Observable<number> {
    return this.numberDisplayedPosts.asObservable();
  }

  getPosts(): Observable<Post[]> {
    return this.postsSubject.asObservable();
  }
  setPosts(posts: Post[]) {
    this.postsSubject.next(posts);
  }

  getSinglePost(): Observable<Post> {
    return this.singlePostSubject.asObservable();
  }
  setSinglePost(post): void {
    this.singlePostSubject.next(post);
  }

  getIsEndPosts(): Observable<boolean> {
    return this.isEndPostsSubject.asObservable();
  }
  setIsEndPosts(value: boolean) {
    this.isEndPostsSubject.next(value);
  }

  requestPosts(): void {
    let transformedPosts: Post[] = [];

    let numberSearchesPosts: number;
    let numberDisplayedPosts: number;

    this.getNumberSearchesPosts().subscribe(value => {
      numberSearchesPosts = value;
    });
    this.getNumberDisplayedPosts().subscribe(value => {
      numberDisplayedPosts = value;
    });

    this.loading.setIsLoading(true);

    this.http.get<Post[]>(urlOptions.posts, httpOptions).subscribe((posts: Post[]) => {
      transformedPosts = this.createPosts(posts, numberSearchesPosts, numberDisplayedPosts);
      transformedPosts = transformedPosts.map(
        post => this.transformationPost(post)
      );

      this.loading.setIsLoading(false);

      this.setPosts(transformedPosts);

      if (transformedPosts.length && numberSearchesPosts >= transformedPosts.length) {
        this.setIsEndPosts(true);
      }

      numberSearchesPosts += numberDisplayedPosts;
      this.setNumberSearchesPosts(numberSearchesPosts);
    });
  }

  getPostById(id: number): void {
    let searchPost;

    this.getPosts().subscribe(posts => {

      if (posts.length) {
        posts.forEach(item => {
          if (item.id === id) {
            searchPost = item;
          }
        });

        searchPost ? this.setSinglePost(searchPost) : this.router.navigate(['/posts']);
      } else {
        this.requsetPostById(id);
      }

    });
  }
  requsetPostById(id) {
    this.http.get<Post>(`${urlOptions.posts}/${id}`, httpOptions).subscribe(post => {
      const transformedPost = this.transformationPost(post as Post);
      this.setSinglePost(transformedPost);
    });
  }

  deletePost(id: number): Observable<any> {
    let posts: Post[];
    this.getPosts().subscribe(value => {
      posts = value.filter(post => post.id !== id);
    });

    return this.http.delete(`${urlOptions.posts}/${id}`).pipe(
      tap(() => this.setPosts(posts))
    );
  }

  udpatePost(post: Post): Observable<Post> {
    return this.http.put<Post>(`${urlOptions.posts}/${post.id}`, post);
  }
  updatePosts(post) {
    let updatedPosts: Post[] = [];

    this.getPosts().subscribe(posts => {
      updatedPosts = posts.map(
        item => {
          if (item.id === post.id) {
            this.setSinglePost(post);
            return post;
          }

          return item;
        });
    });

    this.setPosts(updatedPosts);
  }

  getIsEndCommentsSubject(): Observable<boolean> {
    return this.isEndCommentsSubject.asObservable();
  }
  setIsEndCommentsSubject(value: boolean) {
    this.isEndCommentsSubject.next(value);
  }

  setNumberSearchingComments(value) {
    this.numberSearchingComments.next(value);
  }
  getNumberSearchingComments(): Observable<number> {
    return this.numberSearchingComments.asObservable();
  }
  getNumberDisplayedComments(): Observable<number> {
    return this.numberDisplayedComments.asObservable();
  }

  getPostComments(): Observable<Comment[]> {
    return this.commentsSubject.asObservable();
  }
  setPostComments(value) {
    this.commentsSubject.next(value);
  }

  requestPostComments(id: number, isViewDetailsPage: boolean): Observable<Comment[]> {

    let numberDisplayedComments: number;
    let numberSearchingComments: number;

    this.getNumberDisplayedComments().subscribe(value => {
      numberDisplayedComments = value;
    }).unsubscribe();
    this.getNumberSearchingComments().subscribe(value => {
      numberSearchingComments = value;
    }).unsubscribe();

    return this.http.get<Comment[]>(`${urlOptions.comments}?postId=${id}`, httpOptions).pipe(
      map(
        value => {
          const comments = this.createComments(value, numberSearchingComments, numberDisplayedComments, isViewDetailsPage);
          numberSearchingComments += numberDisplayedComments;
          this.setNumberSearchingComments(numberSearchingComments);

          if (numberSearchingComments > comments.length) {
            this.setIsEndCommentsSubject(true);
          }

          return comments;
        }
      )
    );
  }

  addComment(comment: Comment, id: number): Observable<Comment> {
    let comments: Comment[] = [];
    this.getPostComments().subscribe(value => {
      comments = value;
    });
    comments.push(comment);
    this.setPostComments(comments);
    return this.http.post<Comment>(`${urlOptions.comments}?postId=${id}`, comment, httpOptions);
  }

  deleteComment(id: number): Observable<object> {
    let comments: Comment[] = [];
    this.getPostComments().subscribe(value => {
      comments = value.filter(comment => comment.id !== id);
    });
    this.setPostComments(comments);

    return this.http.delete<object>(`${urlOptions.comments}?id=${id}`);
  }

  likePost(post: Post): Observable<Post> {
    return this.http.patch<Post>(`${urlOptions.posts}/${post.id}`, post, httpOptions);
  }

  requestCreatePost(post): Observable<Post | object> {
    return this.http.post(`${urlOptions.posts}`, post, httpOptions);
  }
  addCreatedPost(post) {
    let posts: Post[] = [];

    this.getPosts().subscribe(value => {
      posts = value;
    });

    this.setPosts([post, ...posts]);
  }

  transformationPost(post: Post): Post {
    const transformedPost = {
      ...post,
      image: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
      valueLikes: post.id,
    };
    return transformedPost as Post;
  }

  createPosts(posts: Post[], numberSearches: number, numberDisplayed: number): Post[] {
    return posts.slice(0, numberSearches + numberDisplayed);
  }

  createComments(comments: Comment[], numberSearchingComments: number, numberDisplayedComments: number, isViewDetailsPage: boolean
  ): Comment[] {
    if (!isViewDetailsPage) {
      return comments.slice(-3);
    }
    return comments.slice(0, numberSearchingComments + numberDisplayedComments);
  }

  createNewCommetn(postId: number, commentId: number, comment: string): Comment {
    return {
      postId,
      id: commentId,
      body: comment,
      name: 'D',
      email: 'random@mail.net'
    };
  }

}
