export class Post {
  id: number;
  userId: number;
  title: string;
  body: string;
  image?: string;
  valueLikes?: number;
}
