export class User {
  sessionToken: string;
  user: {
    login: string,
    password: string;
  };
}
