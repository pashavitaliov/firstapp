import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';

import { MatDialog, MatDialogRef } from '@angular/material';

import { AuthService } from '@app/core/services/auth.service';
import { PostsCreateModalComponent } from '@app/modules/shared/posts/posts-create-modal/posts-create-modal.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements  OnDestroy {
  mobileQuery: MediaQueryList;

  modalRef: MatDialogRef<PostsCreateModalComponent>;

  private mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private authService: AuthService,
    private dialog: MatDialog
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this.mobileQueryListener);
  }

  logout() {
    this.authService.logout();
    // console.log('logout');
  }

  get isLogined() {
    return localStorage.getItem('sessionToken');
  }

  createPostModalOpen() {
    this.modalRef = this.dialog.open(PostsCreateModalComponent, {
      width: '70%',
      maxWidth: '700px',
    } as any);
  }
}
