import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/index';
import { switchMap } from 'rxjs/internal/operators';

import { PostsService } from '@app/core/services/posts.service';
import { Post } from '@app/models/post';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss']
})
export class PostDetailsComponent implements OnInit, OnDestroy {
  selectedId: number;

  post: Post;

  postSubscription$: Subscription;

  constructor(
    private route: ActivatedRoute,
    private postsService: PostsService
  ) {}

  ngOnInit() {
    this.postSubscription$ = this.route.paramMap.pipe(
      switchMap(param => {
        this.selectedId = Number(param.get('id'));
        this.postsService.getPostById(this.selectedId);

        return this.postsService.getSinglePost();
      })
    ).subscribe(post => {
      this.post = post as Post;
    });
  }

  ngOnDestroy() {
    if (this.postSubscription$) {
      this.postSubscription$.unsubscribe();
    }
  }

}
