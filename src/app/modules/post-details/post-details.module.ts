import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostDetailsComponent } from './post-details.component';


import { PostsModule } from '@app/modules/shared/posts/posts.module';

@NgModule({
  declarations: [
    PostDetailsComponent,
  ],
  imports: [
    CommonModule,
    PostsModule
  ],
  exports: [
    PostDetailsComponent
  ]
})
export class PostDetailModule { }
