import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostsListComponent } from './posts-list.component';

import { SharedModule } from '@app/shared/shared.module';
import { PostsModule } from '@app/modules/shared/posts/posts.module';

@NgModule({
  declarations: [
    PostsListComponent,
  ],
  imports: [
    CommonModule,
    PostsModule,
    SharedModule
  ],
  exports: [
    PostsListComponent,
  ]
})
export class PostListModule { }
