import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { animate, group, state, style, transition, trigger } from '@angular/animations';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { throttleTime, tap } from 'rxjs/operators';

import { PostsService } from '@app/core/services/posts.service';
import { Post } from '@app/models/post';
import { LoadingService } from '@app/core/services/loading.service';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss'],
  animations: [
    trigger('shrinkOut', [
      state('in', style({ height: '*' })),
      transition('* => void', [
        style({ height: '*', opacity: 1 }),
        group([
          animate(350, style({ height: 0 })),
          animate(250, style({ opacity: 0 }))
        ])
      ])
    ])
  ]
})
export class PostsListComponent implements OnInit, OnDestroy {
  @ViewChild('indication', { static: true }) indication: ElementRef;

  posts: Post[] = [];
  posts$: Observable<Post[]>;
  postsSubscription$: Subscription;
  isEndPosts$: Observable<boolean>;

  isFetching$: Observable<boolean> = this.loading.getIsLoading();

  srollIndivatorSubscription$: Subscription;

  constructor(
    private postsService: PostsService,
    private loading: LoadingService
  ) {
  }

  ngOnInit() {
    this.getPosts();
    this.indicatorScrolling();
  }

  ngOnDestroy() {
    this.srollIndivatorSubscription$.unsubscribe();
    this.postsSubscription$.unsubscribe();
  }

  getPosts(): void {
    this.posts$ = this.postsService.getPosts();
    this.postsSubscription$ = this.posts$.subscribe(posts => {
      if (!posts.length) {
        this.requestPosts();
      }
    });
    this.isEndPosts$ = this.postsService.getIsEndPosts();
  }

  requestPosts(): void {
    this.postsService.requestPosts();
  }

  indicatorScrolling(): void {
    // const scrollIndication = document.getElementById('indication');
    const scrollIndication = this.indication.nativeElement;

    const getScrollWidth = () => {
      const doc = document.documentElement;
      const winScroll = doc.scrollTop;
      const height = doc.scrollHeight - doc.clientHeight;

      return (winScroll / height) * 100;
    };

    this.srollIndivatorSubscription$ = fromEvent(document, 'scroll')
      .pipe(
        throttleTime(50),
        tap(_ => {
          scrollIndication.style.width = `${getScrollWidth()}%`;
        })
      )
      .subscribe();
  }

  trackById(index: number, post: Post): number {
    return post.id;
  }

}
