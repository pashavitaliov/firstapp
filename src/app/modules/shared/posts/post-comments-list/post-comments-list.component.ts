import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Observable,   Subscription } from 'rxjs/index';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { PostsService } from '@app/core/services/posts.service';
import { Comment } from '@app/models/comment';

@Component({
  selector: 'app-post-comments-list',
  templateUrl: './post-comments-list.component.html',
  styleUrls: ['./post-comments-list.component.scss'],
  animations: [
    trigger('shrinkOut', [
      state('in', style({ height: '*' })),
      transition('* => void', [
        style({ height: '*' }),
        animate(250, style({ height: 0, overflow: 'hidden' }))
      ])
    ])
  ]
})
export class PostCommentsListComponent implements OnInit, OnDestroy {
  @Input() postId: number;
  @Input() isViewDetailsPage: boolean;

  comments: Comment[] = [];
  isFetching = false;

  isEndComments$: Observable<boolean> | boolean = false;

  commentsSubscription$: Subscription;
  commentSubscription$: Subscription;
  deleteCommentSubscription$: Subscription;

  get commetnDate() {
    const date = new Date();
    date.setDate(date.getDate() - 5);
    return date.toLocaleString();
  }

  constructor(
    private postsService: PostsService,
  ) { }

  ngOnInit() {
    this.getComments(this.postId);

    this.isEndComments$ = this.postsService.getIsEndCommentsSubject();
  }

  ngOnDestroy() {
    if (this.commentSubscription$) {
      this.commentSubscription$.unsubscribe();
    }
    if (this.commentsSubscription$) {
      this.commentsSubscription$.unsubscribe();
    }
    if (this.deleteCommentSubscription$) {
      this.deleteCommentSubscription$.unsubscribe();
    }

    this.postsService.setNumberSearchingComments(0);
  }

  getComments(id: number): void {
    this.isFetching = true;
    this.commentsSubscription$ =  this.postsService.requestPostComments(id, this.isViewDetailsPage).subscribe(
      value => {
        this.comments = value;
        this.isFetching = false;
      });
  }

  deleteComment(id: number): void {
    this.postsService.deleteComment(id);
    this.deleteCommentSubscription$ = this.postsService.deleteComment(id).subscribe(e => console.log(e));
  }

  addComment(comment: string): void {
    if (comment === null || !comment.trim()) {
      return;
    }

    const newCommentId = new Date().getTime();

    const newComment = this.postsService.createNewCommetn(this.postId, newCommentId, comment);

    this.commentSubscription$ = this.postsService.addComment(newComment, this.postId).subscribe(
      result => {
        console.log(result);
      }
    );
  }

  trackById(index: number, comment: Comment): number {
    return comment.id;
  }
}
