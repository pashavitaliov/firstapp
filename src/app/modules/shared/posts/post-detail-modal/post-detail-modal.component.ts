import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import {
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs/index';

import { Post } from '@app/models/post';
import { PostsService } from '@app/core/services/posts.service';

import { isEmptyField } from '@app/shared/directives/is-empty-field/is-empty-field.directive';
import { LoadingService } from '@app/core/services/loading.service';

@Component({
  selector: 'app-post-detail-modal',
  templateUrl: './post-detail-modal.component.html',
  styleUrls: ['./post-detail-modal.component.scss']
})
export class PostDetailModalComponent implements OnInit, OnDestroy {
  isEditing = false;

  isFetching$: Observable<boolean> = this.loading.getIsLoading();

  post: Post;

  postEdition: FormGroup;

  updatePostSubscription$: Subscription;
  isEmpty: ValidationErrors | any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<PostDetailModalComponent>,
    private router: Router,
    private postService: PostsService,
    private fb: FormBuilder,
    private loading: LoadingService
  ) {
  }

  ngOnInit() {
    this.post = this.data.post;

    this.isEditing = this.data.openWithEditor;

    this.postEdition = this.fb.group({
      title: this.fb.control(this.post.title, [Validators.required, isEmptyField()]),
      image: this.fb.control(this.post.image, [Validators.required, isEmptyField()]),
      body: this.fb.control(this.post.body, [Validators.required, isEmptyField()]),
    });
  }

  ngOnDestroy() {
    if (this.updatePostSubscription$) {
      this.updatePostSubscription$.unsubscribe();
    }
  }

  editPost() {
    this.isEditing = true;
  }

  onSave() {
    this.loading.setIsLoading(true);
    for (const key of Object.keys(this.postEdition.controls)) {
      const errors = this.postEdition.controls[key].errors as ValidationErrors | any;
      if (errors && errors.isEmpty) {
        this.postEdition.value[key] = this.post[key];
      }
    }

    const postEdited = { ...this.post, ...this.postEdition.value };
    this.postService.updatePosts(postEdited);
    this.updatePostSubscription$ = this.postService.udpatePost(postEdited as Post)
      .subscribe(result => {
        this.loading.setIsLoading(false);
        this.dialogRef.close();
      });

    // this.routeToPost();
  }

  routeToPost() {
    this.dialogRef.close();
    this.router.navigate(['/posts', this.post.id]);
  }


}
