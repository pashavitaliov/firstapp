import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';

@Component({
  selector: 'app-post-field-comment',
  templateUrl: './post-field-comment.component.html',
  styleUrls: ['./post-field-comment.component.scss']
})
export class PostFieldCommentComponent implements OnInit {
  commentField = new FormControl('');

  @Output() comment = new EventEmitter();

  addComment(commentField: NgForm): void {
    this.comment.emit(commentField.value);
    this.commentField.reset();
  }

  constructor() { }

  ngOnInit() {
  }
}
