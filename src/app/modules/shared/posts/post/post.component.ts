import { Component, EventEmitter, Input, OnInit, OnDestroy, Output } from '@angular/core';
import { Observable, Subscription } from 'rxjs/index';
import { Router } from '@angular/router';

import { PostsService } from '@app/core/services/posts.service';
import { Post } from '@app/models/post';
import { MatDialog, MatDialogRef } from '@angular/material';
import { PostDetailModalComponent } from '@app/modules/shared/posts/post-detail-modal/post-detail-modal.component';
import { LoadingService } from '@app/core/services/loading.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit, OnDestroy {
  @Input() isViewDetailsPage;
  @Input() post;
  @Output() likePostChanged = new EventEmitter();

  isFetching$: Observable<boolean> = this.loading.getIsLoading();

  likePostSubscription$: Subscription;
  postSubscription$: Subscription;

  isOpenComments = false;

  modalRef: MatDialogRef<PostDetailModalComponent>;

  constructor(
    private router: Router,
    private postsService: PostsService,
    private dialog: MatDialog,
    private loading: LoadingService
  ) {  }

  ngOnInit() {
  }

  openDialog(openWithEditor = false) {
    this.modalRef = this.dialog.open(PostDetailModalComponent, {
      width: '70%',
      maxWidth: '700px',
      data: {
        openWithEditor,
        post: this.post
      }
    } as any);
  }

  ngOnDestroy() {
    if (this.likePostSubscription$) {
      this.likePostSubscription$.unsubscribe();
    }

    if (this.postSubscription$) {
      this.postSubscription$.unsubscribe();
    }
  }

  openComments(): void {
    this.isOpenComments = !this.isOpenComments;
  }

  deletedPost(id: number): void {
    this.loading.setIsLoading(true);
    this.postSubscription$ = this.postsService.deletePost(id).subscribe(() => {
      this.loading.setIsLoading(false);
      this.router.navigate(['/posts']);
    });
  }

  addLike(likedPost: Post): void {
    this.post.valueLikes = this.post.valueLikes + 1;

    this.likePostSubscription$ = this.postsService.likePost(likedPost)
      .subscribe(post => {
        this.post = post;
        console.log(post);
      });
  }

}

