import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostsCreateModalComponent } from './posts-create-modal.component';

describe('PostsCreateModalComponent', () => {
  let component: PostsCreateModalComponent;
  let fixture: ComponentFixture<PostsCreateModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostsCreateModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsCreateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
