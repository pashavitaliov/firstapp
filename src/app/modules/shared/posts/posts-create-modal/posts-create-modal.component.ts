import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { PostsService } from '@app/core/services/posts.service';
import { Observable, Subscription } from 'rxjs/index';
import { LoadingService } from '@app/core/services/loading.service';

@Component({
  selector: 'app-posts-create-modal',
  templateUrl: './posts-create-modal.component.html',
  styleUrls: ['./posts-create-modal.component.scss']
})
export class PostsCreateModalComponent implements OnInit, OnDestroy {
  postCreate: FormGroup;

  createPostSubscription$: Subscription;

  isFetching$: Observable<boolean> = this.loading.getIsLoading();

  constructor(
    public dialogRef: MatDialogRef<PostsCreateModalComponent>,
    private fb: FormBuilder,
    private postsService: PostsService,
    private loading: LoadingService
  ) { }

  ngOnInit() {
    this.createFrom();
  }

  ngOnDestroy() {
    if (this.createPostSubscription$) {
      this.createPostSubscription$.unsubscribe();
    }
  }

  createFrom() {
    this.postCreate = this.fb.group({
      title: this.fb.control(''),
      image: this.fb.control(''),
      body: this.fb.control(''),
    });
  }

  cancel() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.loading.setIsLoading(true);

    const newPost = {
      ...this.postCreate.value,
      valueLikes: 1,
      userId: 999
    };

    this.createPostSubscription$ = this.postsService.requestCreatePost(newPost).subscribe(e => {
      this.postsService.addCreatedPost(e);
      this.loading.setIsLoading(false);
      this.cancel();
    });
  }

}
