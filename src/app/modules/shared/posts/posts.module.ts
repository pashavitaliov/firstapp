import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PostComponent } from './post/post.component';
import { PostCommentsListComponent } from './post-comments-list/post-comments-list.component';
import { PostFieldCommentComponent } from './post-field-comment/post-field-comment.component';

import { SharedModule } from '@app/shared/shared.module';

import { PostDetailModalComponent } from './post-detail-modal/post-detail-modal.component';


@NgModule({
  declarations: [
    PostComponent,
    PostCommentsListComponent,
    PostFieldCommentComponent,
    PostDetailModalComponent,
    // PostsCreateModalComponent
  ],
  entryComponents: [
    PostDetailModalComponent,
    // PostsCreateModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [
    PostComponent,
    PostCommentsListComponent,
    PostFieldCommentComponent,
    // PostsCreateModalComponent
  ]
})
export class PostsModule { }
