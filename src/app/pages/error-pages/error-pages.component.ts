import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { switchMap } from 'rxjs/internal/operators';

@Component({
  selector: 'app-error-pages',
  templateUrl: './error-pages.component.html',
  styleUrls: ['./error-pages.component.scss']
})
export class ErrorPagesComponent implements OnInit {
  errorPaths = ['/404'];


  constructor(private route: Router) { }

  ngOnInit() {
    const url = this.route.url;
    console.log(url);
    if (this.errorPaths.indexOf(this.route.url) !== -1) {
      this.redirect(url);
      return;
    }
    this.redirect(this.errorPaths[0]);
  }

  redirect(url) {
    this.route.navigate([url]);
  }

}
