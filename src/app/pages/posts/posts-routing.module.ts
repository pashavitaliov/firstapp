import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostsListComponent } from '@app/pages/posts/posts-list/posts-list.component';
import { PostDetailsComponent } from '@app/pages/posts/post-details/post-details.component';

const routes: Routes = [
  { path: '', component: PostsListComponent },
  { path: ':id', component: PostDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule {
}
