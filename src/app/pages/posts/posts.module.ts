import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostsListComponent } from './posts-list/posts-list.component';
import { PostDetailsComponent } from './post-details/post-details.component';

import { PostListModule } from '@app/modules/posts-list/post-list.module';
import { PostDetailModule } from '@app/modules/post-details/post-details.module';
import { SharedModule } from '@app/shared/shared.module';
import { PostsRoutingModule } from './posts-routing.module';

@NgModule({
  declarations: [
    PostsListComponent,
    PostDetailsComponent
  ],
  imports: [
    CommonModule,
    PostListModule,
    PostDetailModule,
    SharedModule,
    PostsRoutingModule
  ],
})
export class PostsModule { }
