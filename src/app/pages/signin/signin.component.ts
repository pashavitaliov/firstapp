import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@app/core/services/auth.service';
import { Observable, Subscription } from 'rxjs/index';
import { LoadingService } from '@app/core/services/loading.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
  providers: []
})
export class SigninComponent implements OnInit, OnDestroy {
  loginForm: FormGroup = this.fb.group({
    email: ['eve.holt@reqres.in', Validators.required],
    password: ['cityslicka', Validators.required],
    viewPassword: false
  });

  isFetching$: Observable<boolean> = this.loading.getIsLoading();

  isViewPassword = false;

  authSubscription$: Subscription;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private loading: LoadingService
  ) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.authSubscription$) {
      this.authSubscription$.unsubscribe();
    }
  }

  onSubmit(e) {
    this.loading.setIsLoading(true);

    const user = this.loginForm.value;
    delete user.viewPassword;

    this.authSubscription$ = this.authService.login(user).subscribe(() => {
      this.loading.setIsLoading(false);
    });
  }
}
