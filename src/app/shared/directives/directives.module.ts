import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HighlightDirective } from './highlight/highlight.directive';
import { IsEmptyFieldDirective } from './is-empty-field/is-empty-field.directive';

const directives = [
  HighlightDirective,
  IsEmptyFieldDirective
];

@NgModule({
  declarations: directives,
  imports: [CommonModule],
  exports: directives
})
export class DirectivesModule { }
