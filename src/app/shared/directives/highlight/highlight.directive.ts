import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  @Input('appHighlight') highlightColor;
  @Input() defaultColor: string;

  color = 'yellow';
  colorRandom: string;

  timerId: number;

  constructor(private el: ElementRef) {
  }


  @HostListener('mouseenter') onMouseEnter() {
    // this.highlight(this.highlightColor || this.defaultColor || this.color);

    // this.colorRandom = `#${Math.floor(Math.random() * 16777215).toString(16)}`;
    // console.log(this.colorRandom);

    this.timerId = setInterval(() => {
      this.colorRandom = `#${Math.floor(Math.random() * 16777215).toString(16)}`;

      this.highlight(this.colorRandom);
    }, 200);
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.highlight(null);
    clearInterval(this.timerId);
  }

  private highlight(color: string) {
    this.el.nativeElement.style.background = color;
  }
}
