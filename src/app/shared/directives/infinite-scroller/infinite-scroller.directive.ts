import { Directive, AfterViewInit, ElementRef, Input, OnInit } from '@angular/core';

import { Observable, Subscription, fromEvent, from } from 'rxjs/index';
import { map, pairwise, exhaustMap, filter, startWith } from 'rxjs/internal/operators'
import { Router } from '@angular/router';

interface ScrollPosition {
  sH: number;
  sT: number;
  cH: number;
}

const DEFAULT_SCROLL_POSITION: ScrollPosition = {
  sH: 0,
  sT: 0,
  cH: 0
};

@Directive({
  selector: '[appInfiniteScroller]'
})
export class InfiniteScrollerDirective implements AfterViewInit, OnInit {

  private scrollEvent$;

  private userScrolledDown$;

  private requestStream$;

  private requestOnScroll$;

  @Input()
  scrollCallback;

  @Input()
  immediateCallback;

  @Input()
  scrollPercent = 70;

  constructor(
    private elm: ElementRef,
    private router: Router
  ) {
  }

  ngOnInit() {
    console.log(this.elm.nativeElement);
    // this.registerScrollEvent();
  }

  ngAfterViewInit() {

    this.registerScrollEvent();

    // this.streamScrollEvents();

    // this.requestCallbackOnScroll();

  }

  private registerScrollEvent() {
    // const source = fromEvent(this.elm.nativeElement, 'click')
    //   .subscribe(e => console.log(e));
    //
    // fromEvent(this.elm.nativeElement, 'scroll').subscribe(e => console.log(e));

    this.scrollEvent$ = fromEvent(this.elm.nativeElement, 'scroll');
    this.scrollEvent$.subscribe( e => console.log(e));
    //
    // this.userScrolledDown$ = this.scrollEvent$.pipe(
    //   map(e => {
    //     console.log(e);
    //     return e;
    //   }),
    //   pairwise(),
    //   filter(positions => this.isUserScrollingDown(positions) && this.isScrollExpectedPercent(positions[1]))
    // );
    //
    // this.userScrolledDown$.subscribe(e => console.log(e));

  }

  private streamScrollEvents() {
    this.userScrolledDown$ = this.scrollEvent$.pipe(
      map((e: any): ScrollPosition => ({
        sH: e.target.scrollHeight,
        sT: e.target.scrollTop,
        cH: e.target.clientHeight
      })),
      pairwise(),
      filter(positions => this.isUserScrollingDown(positions) && this.isScrollExpectedPercent(positions[1]))
    );

    this.userScrolledDown$.subscribe(e => console.log(e))
  }

  private requestCallbackOnScroll() {

    this.requestOnScroll$ = this.userScrolledDown$;

    if (this.immediateCallback) {
      this.requestOnScroll$ = this.requestOnScroll$.pipe(
        startWith([DEFAULT_SCROLL_POSITION, DEFAULT_SCROLL_POSITION])
      );
    }

    this.requestOnScroll$.pipe(
      exhaustMap(() => this.scrollCallback())
    ).subscribe(() => {
    });

  }

  private isUserScrollingDown = positions => {
    // console.log(positions);
    return positions[0].sT < positions[1].sT;
  }

  private isScrollExpectedPercent = (position) => {
    // console.log(position);
    return ((position.sT + position.cH) / position.sH) > (this.scrollPercent / 100);
  }

}
