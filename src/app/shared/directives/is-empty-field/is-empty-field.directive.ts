import {
  Directive,
  Input
} from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidatorFn
} from '@angular/forms';

export function isEmptyField(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const isEmpty = control.value.trim();
    return !isEmpty ? { isEmpty: true } : null;
  };
}

@Directive({
  selector: '[appIsEmptyField]',
  providers: [{ provide: NG_VALIDATORS, useExisting: IsEmptyFieldDirective, multi: true }]
})
export class IsEmptyFieldDirective {
  @Input('appIsEmptyField') emptyField: string;

  validate(control: AbstractControl): {[key: string]: any} | null {
    console.log(control);
    console.log(isEmptyField()(control));
    return this.emptyField ? isEmptyField()(control)
      : null;
  }

  constructor() { }

}
