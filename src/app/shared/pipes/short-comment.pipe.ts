import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortComment'
})
export class ShortCommentPipe implements PipeTransform {

  transform(comment: string, lengthStr: number): string {
    return comment.length > lengthStr ? `${comment.slice(0, lengthStr)} ...` : comment;
  }

}
