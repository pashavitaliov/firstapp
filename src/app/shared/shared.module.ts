import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from './material/material.module';
import { DirectivesModule } from './directives/directives.module';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ShortCommentPipe } from './pipes/short-comment.pipe';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';

@NgModule({
  declarations: [
    ShortCommentPipe,
    LoadingSpinnerComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    MaterialModule,
    DirectivesModule,
    InfiniteScrollModule,
    ShortCommentPipe,
    LoadingSpinnerComponent,
  ]
})
export class SharedModule { }
